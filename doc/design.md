# alforge

## objectives

1. build an user-space filesystem;

2. learn rust language;

## filesystem design

txn: DYNAMO (total order)
  * transaction/ID

  * transaction/PARENT
  * transaction/TIMELINE
  * transaction/DATETIME

root-node: DYNAMO
  * root-node/id: ID

  * root-node/magic: uint16
  * root-node/size: uint64
  * root-node/root: stat-node/ID

stat-node: DYNAMO
  * stat-node/id: ID

  * stat-node/magic: uint16
  * stat-node/size: {txn: uint64}
  * stat-node/data: {txn: [data-node/id]}

name-node: DYNAMO
  * name-node/id: ID

  * name-node/name: string
  * name-node/stat: {txn: maybe stat-node/id}

data-node: S3
  * data-node/id: ID

  * data-node/magic: uint16
  * data-node/data: [u8]


stat-node:
  * id: STAT-0
  * data: {TXN: [DATA-0]}

stat-node:
  * id: STAT-1
  * data: {TXN: [DATA-1]}

data-node:
  * id: DATA-0
  * type: F
  * data: [(A, STAT-1)]

data-node:
  * id: DATA-1
  * type: B
  * data: file-content

OPERATIONS

rename-file(old-path, new-path):
  let old-dir-inode = stat-parent old-path
  let new-dir-inode = stat-parent new-path

  atomically
    dissoc old-dir-inode old-path
    assoc new-dir-inode new-path

create-file(path):
  let dir-inode = stat-parent path
  atomically
    require not-exists path
    let inode = create-inode
    insert dir-inode path inode

unlink(path):
  let dir-inode = stat-parent path
  atomically
    require is-file path
    dissoc dir-inode path

rmdir(path):
  let dir-inode = stat-parent path
  atomically
    require is-directory path
    require is-empty path
    dissoc dir-inode path
