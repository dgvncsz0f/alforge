pub enum ErrorKind {
  IoError,
}

pub enum ErrorCode {
  NotFound,
}

pub struct Error {
  kind: ErrorKind,
  code: ErrorCode,
  reason: String,
}

pub fn kind_of(error: &Error) -> &ErrorKind {
  &error.kind
}

pub fn code_of(error: &Error) -> &ErrorCode {
  &error.code
}

pub fn reason(error: &Error) -> &str {
  &error.reason
}

pub fn io_error_not_found(reason: Option<&str>) -> Error {
  Error {
    kind: ErrorKind::IoError,
    code: ErrorCode::NotFound,
    reason: match reason {
      None => "io-error/not-found".to_string(),
      Some(text) => text.to_string(),
    },
  }
}
