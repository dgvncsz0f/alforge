use crate::data::block::Block;
use crate::data::block::BlockView;
use crate::data::ident::Ident;
use crate::error::Error;

/// the most basic storage unit for blocks. it allows saving, loading
/// and stating blocks (querying for existence).
pub trait Blockdev {
  /// Save a block which must later be referenced by its `Ident`.
  /// This must be a synchronous operation w.r.t. to `load` and
  /// `stat`. Also, it is expected the implementations of this trait
  /// to provide read-after-write semantics. Given an `Ident`, `load`
  /// and `stat` must return the contents of the latest `save`.
  fn store(&mut self, block: Block) -> Result<(), Error>;

  /// Retrieves a previous saved block by its Ident.
  fn load<'t>(&'t self, ident: &Ident) -> Result<BlockView<'t>, Error>;

  /// Check if a block is currently stored in this block device.
  fn exists(&self, ident: &Ident) -> Result<bool, Error>;

  /// Removes a block from the device. It is considered an error to
  /// remove a block that does not exist.
  fn delete(&mut self, ident: &Ident) -> Result<(), Error>;
}

pub trait Namedev {
  fn store(&mut self, name: &str, block: Block) -> Result<(), Error>;

  fn load<'t>(&'t self, name: &str) -> Result<BlockView<'t>, Error>;

  fn exists(&self, name: &str) -> Result<bool, Error>;

  fn delete(&mut self, name: &str) -> Result<(), Error>;
}
