use crate::data::block::Block;
use crate::data::block::BlockView;
use crate::data::block::Bytes;
use crate::data::ident::Ident;
use crate::dev::blockdev::Blockdev;
use crate::error;
use std::collections::HashMap;

/// This is an implementation of blockdevice that keeps data
/// in-memory. Mostly used for testing.
pub struct Memory {
  pub names: HashMap<String, Ident>,
  pub blocks: HashMap<Ident, Bytes>,
}

impl Memory {
  pub fn new() -> Memory {
    Memory {
      blocks: HashMap::new(),
      names: HashMap::new()
    }
  }
}

impl Blockdev for Memory {
  fn store(&mut self, block: Block) -> Result<(), error::Error> {
    self.blocks.insert(block.ident, block.bytes);

    Ok(())
  }

  fn load<'t>(&'t self, ident: &Ident) -> Result<BlockView<'t>, error::Error> {
    match self.blocks.get_key_value(ident) {
      Some((ident, value)) => Ok(BlockView {
        ident: ident,
        bytes: value,
      }),

      None => Err(error::io_error_not_found(None)),
    }
  }

  fn exists(&self, ident: &Ident) -> Result<bool, error::Error> {
    Ok(self.blocks.contains_key(ident))
  }

  fn delete(&mut self, ident: &Ident) -> Result<(), error::Error> {
    if self.blocks.contains_key(ident) {
      self.blocks.remove(ident);

      Ok(())
    } else {
      Err(error::io_error_not_found(None))
    }
  }
}
