use crate::data::block::Block;
use crate::data::block::BlockView;
use crate::data::block::Ident;
use crate::error::Error;

pub trait Namedev {
  fn store(&mut self, ident: &str, block: Block) -> Result<(), Error>;

  fn load(&'t self, ident: &str) -> Result<BlockView<'t>, Error>;

  fn exists(&self, ident: &str) -> Result<bool, Error>;

  fn delete(&mut self, ident: &str) -> Result<(), Error>;
}
