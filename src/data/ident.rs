use blake2::Blake2b;
use blake2::Digest;

/// Ident is an array of 512bits currently implemented using BLAKE2b
/// hash function.
pub type Ident = [u128; 4];

pub fn from_bytes(bytes: &[u8]) -> Ident {
  let digest = Blake2b::digest(&bytes);
  if 64 != digest.len() {
    panic!("data/ident#from_bytes");
  }

  let mut ident = [0u128; 4];
  for (i, &x) in digest.iter().enumerate() {
    let n = i / 16;
    let m = i % 16 * 8;

    ident[n] = ident[n] | u128::from(x) << m;
  }

  ident
}

#[cfg(test)]
mod tests {
  use super::*;
  use rand::RngCore;

  #[test]
  fn from_bytes_value() {
    let mut data = [0u8; 1024];
    rand::thread_rng().fill_bytes(&mut data);

    let digest = Blake2b::digest(&data);
    let ident = from_bytes(&data);
    for i in 0..64 {
      let n = i / 16;
      let m = i % 16;

      assert_eq!(u128::from(digest[i]), (ident[n] >> m * 8) & 0xff);
    }
  }
}
