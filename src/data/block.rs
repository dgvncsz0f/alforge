use crate::data::ident;
use crate::data::ident::Ident;

pub type Bytes = Vec<u8>;

pub struct Block {
  pub ident: Ident,
  pub bytes: Bytes,
}

pub struct BlockView<'t> {
  pub ident: &'t Ident,
  pub bytes: &'t Bytes,
}

impl Block {
  pub fn new(bytes: Bytes) -> Block {
    let ident = ident::from_bytes(&bytes);

    Block {
      ident: ident,
      bytes: bytes,
    }
  }
}
