pub trait Namedev {
  fn store(&mut self, name: & str, ident: & Ident) {
  }

  fn load(&mut self, name: &str) -> Result<Ident, Error> {
  }

  fn exists(&self, name: &str) -> Result<bool, Error> {
  }

  fn delete(&mut self, ident: &Ident) -> Result<(), Error> {
  }
}
